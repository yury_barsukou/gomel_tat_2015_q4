package com.epam.tat.module4.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

/**
 * Created by Aleh_Vasilyeu on 11/19/2015.
 */
public class MultAndDivTest extends BaseCalculatorTest {

    @Test
    public void divOneValueByAnother() {
        double result = calculator.div(3D, 2D);
        Assert.assertEquals(result, 1.5D, "Invalid result of operation");
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void checkDivisionByZeroThrowsException() {

        throw new RuntimeException();
        //double result = calculator.div(3D, 0D);
    }
}
