package com.epam.tat.module4.test.junit;

import com.epam.tat.module4.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.AllTests;
import org.junit.runners.JUnit4;


/**
 * Created by Aleh_Vasilyeu on 11/19/2015.
 */
@RunWith(JUnit4.class)
public class MultAndDivTest {

    private Calculator calculator = new Calculator();

    @Test
    public void testDivOneValueByAnother() {
        double result = calculator.div(3D, 2D);
        Assert.assertEquals(result, 1.5D, 0.1);
    }

    @Test(expected = NumberFormatException.class)
    public void testCheckDivisionByZeroThrowsException() {
        double result = calculator.div(3D, 0D);
    }
}
