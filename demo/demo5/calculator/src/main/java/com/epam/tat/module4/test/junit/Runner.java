package com.epam.tat.module4.test.junit;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * Created by Aleh_Vasilyeu on 12/14/2015.
 */
public class Runner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(MultAndDivTest.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }
}
