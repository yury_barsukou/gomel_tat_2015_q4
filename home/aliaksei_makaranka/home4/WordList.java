package com.gomel.tat.home4;

import java.io.*;
import java.util.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class WordList {

    private static  List<String> values = new ArrayList<String>();
    private static Map<String, Integer> words = new TreeMap<String, Integer>();

    void getWords(String baseDirectory, String fileNamePattern) {

        File f = new File(baseDirectory);
        if(!f.exists())
        {
            System.out.println("Not folder.");
            return;
        }
        if (!f.isDirectory())
        {
            System.out.println("Not directory.");
            return;
        }

        try {
            System.setOut(new PrintStream(System.out, true, "Cp866"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String[] DirList = f.list();
        for (int i = 0; i < DirList.length; i++)
        {
            File f1 = new File(baseDirectory + "\\" + DirList[i]);
            if (!f1.isFile())
                getWords(baseDirectory + "\\" + DirList[i], fileNamePattern);
            else if (f1.getName().equals(fileNamePattern)) {
                try {
                    List<String> lines = Files.readAllLines(Paths.get(f1.getPath()), Charset.defaultCharset());
                    for(String line : lines) {
                        String[] tempWords = line.replaceAll("[-\\\\+\\\\.\\\\^:,\"()'!\\d]", "").replaceAll("\t", " ").split(" ");
                        for(String tempWord : tempWords) {
                            if (tempWord.length() > 1 && !tempWord.replaceAll(" ", "").equals(""))
                                values.add(tempWord);
                        }
                    }
                    lines.clear();
                }
                catch (IOException ex){
                }
            }
        }
    }

    public static void createMap() {
        for (String value : values) {
            int i = words.containsKey(value) ? words.get(value) : 0;
            words.put(value, ++i);
        }
    }

    public static void printStatistics() {
        for(Map.Entry value : words.entrySet()) {
            System.out.println(value.getValue() + " " + value.getKey());
        }
    }
}
