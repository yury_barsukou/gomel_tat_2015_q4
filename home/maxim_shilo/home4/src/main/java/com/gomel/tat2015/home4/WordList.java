package com.gomel.tat2015.home4;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class WordList {

    private Map<String, Integer> values = new HashMap<String, Integer>();

    public void printStatistics() {
        for (Map.Entry<String, Integer> entry : values.entrySet()) {
            System.out.println("Word '" + entry.getKey() + "' found " + entry.getValue() + " times.");
        }
    }

    public void getWords(String baseDirectory, String fileNamePattern) {
        File startDirectory = new File(baseDirectory);
        File[] filesInStartDir = startDirectory.listFiles();
        if (filesInStartDir == null)
            return;
        for (File f : filesInStartDir) {
            if (f.isDirectory())
                getWords(f.getPath(), fileNamePattern);
            else if (f.getPath().matches(
                    fileNamePattern.replace(".", "[.]").replace("*", "(.*)").replace("/", "\\\\"))) {
                try {
                    String temp;
                    BufferedReader reader = new BufferedReader(
                            new FileReader(f.getAbsolutePath()));
                    while ((temp = reader.readLine()) != null) {
                        String[] tempArray = (temp.replaceAll("[^а-яА-Я a-zA-Z]", "").split("\\s+"));
                        for (String s : tempArray) {
                            if (s.length() > 1) {
                                addToMap(s);
                            }
                        }
                    }
                    reader.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    public void addToMap(String keyWord) {
        if (values.containsKey(keyWord)) {
            int count = values.get(keyWord) + 1;
            values.put(keyWord, count);
        } else {
            values.put(keyWord, 1);
        }
    }
}
