package com.gomel.tat.home4;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WordList {
    public static final String DIR_WITH_HELLO = "d:\\TAT\\gomel_tat_2015_q4\\home";
    public static void main(String[] args) throws IOException {
        Scanner scanner;
        ArrayList<String> words = new ArrayList<String>();
        ArrayList<Integer> count = new ArrayList<Integer>();
        Path dirPath = FileSystems.getDefault().getPath(DIR_WITH_HELLO);
        List<File> files = new ArrayList<File>();
        getFilesToProcess(files, dirPath);
        for (File file : files){
            scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String nextWord = scanner.next();
                if(words.contains(nextWord)){
                    int index = words.indexOf(nextWord);
                    count.set(index, count.get(index)+1);
                }
                else{
                    words.add(nextWord);
                    count.add(1);
                }
            }
            scanner.close();
        }
        for(int i = 0; i < words.size(); i++){
            System.out.println(words.get(i) + " occured " + count.get(i) + " time(s)");
        }
    }

    private static void getFilesToProcess(List<File> files, Path dirPath) {
        DirectoryStream<Path> folders;
        try {
            folders = Files.newDirectoryStream(dirPath);
            for (Path entry : folders) {
                if (Files.isDirectory(entry)) {
                    getFilesToProcess(files, entry.toAbsolutePath());
                } else {
                    files.add(entry.toFile());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
