import org.apache.commons.io.IOUtils;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Artsiom Hryharenka on 11.12.2015.
 */
public class WordList {
    private static List<String> wordList = new LinkedList<String>();

    public static void counter() {
        Map<String, Integer> words = new HashMap<String, Integer>();
        for (String word : wordList) {
            if (!words.containsKey(word)){
                words.put(word, 1);
            }
            else{
                int count = words.get(word);
                count++;
                words.put(word, count);
            }
        }
        for (String word : words.keySet()) {
            System.out.println(word + " : " + words.get(word));
        }

    }

    public static void getWords(String baseDirectory, String fileNamePattern) throws IOException {
        File folder = new File(baseDirectory);
        File[] files = folder.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    getWords(file.getPath(), fileNamePattern);
                } else {
                    if (file.getPath().endsWith(fileNamePattern)) {
                        parseWords(file);
                    }
                }
            }
        }
    }

    private static void parseWords(File file) throws IOException {
        FileInputStream stream = new FileInputStream(file.getPath());
        try {
            String words = IOUtils.toString(stream);
            Pattern pattern = Pattern.compile("[a-zA-Zа-яА-Я]+");
            Matcher matcher = pattern.matcher(words);
            while (matcher.find()) {
                wordList.add(words.substring(matcher.start(), matcher.end()));
            }
        } finally {
            stream.close();
        }
    }
}
